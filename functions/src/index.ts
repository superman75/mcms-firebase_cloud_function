import * as functions from 'firebase-functions';
const admin = require("firebase-admin");
const os = require('os');
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');

const serviceAccount = require("../serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
const bucketName = 'gs://mcms-ce38b.appspot.com';
const bucket = admin.storage().bucket(bucketName);

// const passport = require('passport');
// const GoogleStrategy = require('passport-google-oauth20').Strategy;

// function extractProfile(profile:any) {
//   let imageUrl = '';
//   if (profile.photos && profile.photos.length) {
//     imageUrl = profile.photos[0].value;
//   }
//   return {
//     id: profile.id,
//     displayName: profile.displayName,
//     image: imageUrl,
//   };
// }

// passport.use(new GoogleStrategy({
//     clientID: '224575117177-vg78ocsc8tbalhrl8bcc3arcdp64vppn.apps.googleusercontent.com',
//     clientSecret: 'Dqv-w4u1d-AvMvvt7D4h1qeg',
//     callbackURL: 'http://localhost:8080/auth/google/callback',
//     accessType: 'offline',
//     userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
//   },
//   function(accessToken: any, refreshToken: any, profile: any, cb: any) {
//     cb(null, extractProfile(profile));
//   }
// ));



// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

/**
* Delete old storage files and save firestore data to storage
**/
export const saveFirestoreToStorage = functions.https.onRequest(async (request, response) => {


	// get all JSON files
	const prefix = "json/";
	const options = {
		prefix: prefix,
	};
	const [files] = await bucket.getFiles(options);
	

	// Delete old storage files
	files.forEach(async (file:any) => {
		// delete 7 days ago data
		const SevendaysAgo = new Date().getTime() - 7 * 24 * 60 * 60 * 1000;
		const filterName = `json/${SevendaysAgo}.json`;

		// // delete 1 hour ago data for testing
		// const oneHourAgo = new Date().getTime() - 60 * 60 * 1000;
		// const filterName = `json/${oneHourAgo}.json`;

		if(file.name<filterName){
			console.log(`Deleting ${file.name}`);
			await bucket.file(file.name).delete();
		}
	});


	// get all collections from firestore
	const collections = await admin.firestore().listCollections()

	// json for storing data from firestore
	const jsonData :any = {};

	let count = 0;
	for(const collection of collections) {
		collection.get().then((docs:any) => {
			const tempArray: any[] = [];
			docs.forEach((doc: any)=>{
				const tempObj : any = {};
				tempObj[doc.id] = doc.data();
				tempArray.push(tempObj);
			});
			jsonData[collection.id] = tempArray;
		}).then(()=>{
			count++;

			// when all collections are stored to json
			if(count === collections.length){

				// create temp file to store json
				const currentTime = new Date().getTime()
				const fileName = `${currentTime}.json`;
				const tempFilePath = path.join(os.tmpdir(), fileName);

				// save json to temp file
				fs.writeFile(tempFilePath, JSON.stringify(jsonData), (err:any)=>{
					if(err){
						response.status(404).send("Error inside write : " + err.message)
					}

					// upload saved tempfile to storage
					const thumbFilePath = path.join("json", fileName);
					bucket.upload(tempFilePath,{
						destination: thumbFilePath
					})
					.then(()=>{

						// delete tempfile
						fs.unlinkSync(tempFilePath);
						response.send("JSON file is uploaded successfully.");
					}).catch((err1:any) => response.status(404).send("Error inside upload : " + err1.message));	
				})
					

			}
		})
	}

});

/**
* retrive json files from certain date and save to Firestore
**/
export const restoreJsonToFirestore = functions.https.onRequest(async(request, response) => {
	
	const regex = /(((((0[1-9]|1[0-9]|2[0-9]|30)|31)(0[13789]|(10|12)))|(((0[1-9]|1[0-9]|2[0-9]|30))(0[34569]|11))|(((0[1-9]|1[0-9]|2[0-7])|(28|29))02))(0?[0-9]|[1-9][0-9]){2})/gm;
	const ddmmyy = request.query.d;
	const email = request.query.email;
	const password = request.query.password;

	if(!regex.test(ddmmyy)) {
		response.status(404).send("date param is invalid");
		return;
	}

	if(email !== "nesau684@gmail.com" || password !== "password123"){
		response.status(404).send("Authentication failed.");
		return;
	}

	const dd = Number(ddmmyy.substring(0,2));
	const mm = Number(ddmmyy.substring(2,4)) - 1;
	let yy = 0;
	if (ddmmyy.length === 6)  yy = Number("20" + ddmmyy.substring(4,6));
	else  yy = Number("2" + ddmmyy.substring(4,7));
	const filterDate = new Date(yy,mm,dd,0,0,0,0).getTime();
	const filterName = `json/${filterDate}.json`;


	// get all JSON files
	const prefix = "json/";
	const options = {
		prefix: prefix,
	};
	const [files] = await bucket.getFiles(options);


	let count = 0;
	files.forEach(async (file:any) => {

		if(file.name>filterName){
			// await file.makePublic();
			const signedUrls: any[] = await file.getSignedUrl({
				action: 'read',
	  			expires: '03-09-2491'
			});
			const collections: any = await rp({
				uri: signedUrls[0],
				json: true
			})
			for (const key in collections) {
			    // skip loop if the property is from prototype
			    if (!collections.hasOwnProperty(key)) continue;

			    const docs:any[] = collections[key];
			    let printString = key;
			    for(const doc of docs){
			    	 for (const docId in doc) {
				        // skip loop if the property is from prototype
				        if(!doc.hasOwnProperty(docId)) continue;
				        printString += " " + docId;

				        await admin.firestore().collection(key).doc(docId).update(doc[docId]);
				    }
			    }
			    console.log(printString);
			}
			
		}

		count++;
		if(count === files.length){
			response.send("Completed!");
		}
	});

});
